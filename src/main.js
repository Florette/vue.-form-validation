import Vue from 'vue';
import App from './App.vue';
import './registerServiceWorker';
import router from './router';
import store from './store';

Vue.mixin({
    computed: {
        $popup() {
            return this.$store.getters.GET_POPUP; // Глобальная видимость через миксин (состояние попап)
        }
    }
});
Vue.config.productionTip = false;

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');

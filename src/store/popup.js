//Состояние
const state = {
    show: false
};
//Передается состояние всем заинтересованным компонентам
const getters = {
    GET_POPUP: state => state
};
//Синхронное изменение состояния
const mutations = {
    SET_SHOW_POPUP: state => state.show = true,
    SET_HIDE_POPUP: state => state.show = false
};
//Асинхронное изменние состояния (ожидания с свервера и т.д.)
const actions = {};

export default {
    state,
    getters,
    mutations,
    actions
}